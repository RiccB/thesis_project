"""
Library containing functions written during the thesis project.
The functions usage ranges from preprocessing to graph generation, 
in no specific order.
Some functions were only used in the exploratory phase 
(e.g all the functions related to Gaussian mixture models), 
and therefore not relevant for the final project.
"""

import datetime
import json
import os
import pickle
import re

from itertools import groupby
from operator import itemgetter
from time import time

from scipy.stats import norm
from sklearn import manifold
from sklearn import mixture
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import pandas as pd
import seaborn as sns


def datToCSV(path):
    """
    Convert file(s) in path from dat to csv.
    """
    if os.path.isfile(path):
        fileToCSV(path)
    elif os.path.isdir(path):
        folderToCSV(path)


def fileToCSV(filePath):
    """
    Converts a dat file to a csv file.
    """

    with open(filePath) as fileToRead:
        fileToWrite = open(filePath.replace('.dat', '.csv'), 'w')
        for line in fileToRead:
            # If the string matches the format number-space-number,
            # Then substitute it with number-comma-number
            if re.match(r'(\d+)\s(\d+)', line):
                fileToWrite.write(
                    re.sub(r'(\d+)\s(\d+)', r'\1,\2', line).strip(' '))
        fileToWrite.close()


def folderToCSV(folderPath):
    """
    Converts all dat files in a folder to csv files.
    """

    for file in os.listdir(folderPath):
        if file.endswith(".dat"):
            fileToCSV(os.path.join(folderPath, file))


def csvToPickle(path):
    """
    Convert file(s) in the path from csv to pickle.
    """
    if os.path.isfile(path):
        fileToPickle(path)
    elif os.path.isdir(path):
        folderToPickle(path)


def fileToPickle(filePath):
    """
    Converts a csv file to a time series dataframe.
    And then saves it as a pickle.
    """
    df = shapeData(filePath)
    # df = addNan(df, minutes)
    df.to_pickle(filePath.replace('.csv', '.pickle'))


def folderToPickle(folderPath):
    """
    Converts all csv files in a folder to time series dataframes.
    And then saves them as pickle.
    """
    for file in os.listdir(folderPath):
        if file.endswith(".csv"):
            filePath = os.path.join(folderPath, file)
            fileToPickle(filePath)


def csvEventToPickle(path):
    """
    Converts event file(s) from csv to pickle.
    """
    if os.path.isfile(path):
        csvEventFileToPickle(path)
    elif os.path.isdir(path):
        csvEventFolderToPickle(path)


def csvEventFileToPickle(filePath):
    """
    Converts event file from csv to pickle.
    """
    df = pd.read_csv(filePath)
    df.columns = ['Time', 'Event']
    df['Time'] = pd.to_datetime(df['Time'], unit='s')
    df.index = df['Time']
    df = df.drop('Time', axis=1)
    df.to_pickle(filePath.replace('.csv', '.pickle'))


def csvEventFolderToPickle(folderPath):
    """
    Converts all event files in folder from csv to pickle.
    """
    for file in os.listdir(folderPath):
        if file.endswith(".csv"):
            filePath = os.path.join(folderPath, file)
            csvEventFileToPickle(filePath)


def readPickle(path):
    """
    Read pickle file(s) in path.
    """
    if os.path.isfile(path):
        return pd.read_pickle(path)
    elif os.path.isdir(path):
        dataframes = {}
        for file in os.listdir(path):
            if file.endswith(".pickle"):
                dataframes[file.replace('.pickle', '')] = pd.read_pickle(
                    os.path.join(path, file))
        # In this case return a dictionary {'NameOfPickle': contentOfPickle}
        # where contentOfPickle is a DataFrame
        return dataframes


def shapeData(filePath):
    """
    Convert a csv file to a time series data frame.
    """
    df = pd.read_csv(filePath)
    df.columns = ['Time', 'Watts']
    df['Time'] = pd.to_datetime(df['Time'], unit='s')
    df.index = df['Time']
    df = df.drop('Time', axis=1)

    return df


def addNan(dataFrame, minutes):
    """
    Substitute the data points at the extremes of a gap
    with NaN to plot gaps that show the interruption.
    """
    differences = dataFrame.index.to_series().diff()
    differences = differences[differences >
                              datetime.timedelta(minutes=minutes)]
    print('Total length of the gaps in the data: '+str(differences.sum())
          + ' out of ' + str(dataFrame.index[-1] - dataFrame.index[0]))
    # print(differences.sum())
    # Loop on all differences to get indices
    for counter, _ in enumerate(differences.index):
        index = dataFrame.index.searchsorted(differences.index[counter])
        dataFrame[index-1:index+1] = np.nan

    return dataFrame


def getTimeExtremes(path):
    """
    Get the first and the last date of the data points in a channel(s).
    """
    if os.path.isfile(path):
        getTimeExtremesFile(path)
    elif os.path.isdir(path):
        getTimeExtremesFolder(path)


def getTimeExtremesFolder(folderPath):
    """
    Get the timestamp of the first and last data point
    for all the files in a folder.
    """
    channels = {}
    for file in os.listdir(folderPath):
        if file.endswith(".dat"):
            channels[file.replace('.dat', '')] = getTimeExtremesFile(
                os.path.join(folderPath, file))


def getTimeExtremesFile(filePath):
    """
    Get the timestamp of the first and last data point in a file
    """
    with open(filePath, 'r') as f:
        lines = f.read().splitlines()
        first = lines[0].partition(' ')[0]
        first = datetime.datetime.fromtimestamp(int(first)).strftime(
            '%Y-%m-%d %H:%M:%S')
        last = lines[-1].partition(' ')[0]
        last = datetime.datetime.fromtimestamp(int(last)).strftime('%Y-%m-%d %H:%M:%S')

    return [first, last]


def drawSeparatedSubplots(dataframes):
    """
    Draw data frames in different sub-plots.
    """
    _, axes = plt.subplots(nrows=len(dataframes), ncols=1, squeeze=False,
                           sharex=True, sharey=False)
    keys = []
    i = 0
    for key, value in dataframes.items():
        print('Plotting '+key)
        keys.append(key)
        value.plot(ax=axes[i, 0])
        axes[i, 0].legend((key, ))
        i += 1
    plt.show()


def drawSingleSubplot(dataframes):
    """
    Draw data frame(s) in single plot.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if len(dataframes) == 1:
        dataframes.plot(ax=ax)
        plt.show()
    elif len(dataframes) > 1:
        keys = []
        for key, value in dataframes.items():
            print(key)
            keys.append(key)
            value.plot(ax=ax)
        ax.legend(keys)
        plt.show()


def drawWattAgainstEvent(dataframes):
    """
    Draw plot of raw data above the plot of events.
    """
    _, axes = plt.subplots(nrows=len(dataframes), ncols=1, squeeze=False,
                           sharex=True, sharey=False)
    keys = []
    i = 0
    for key, value in dataframes.items():
        print('Plotting '+key)
        keys.append(key)
        if i % 2 is 0:
            value.plot(ax=axes[i, 0])
        else:
            # print(value)
            value.plot(ax=axes[i, 0], style='.')
        axes[i, 0].legend((key, ))
        i += 1
    plt.show()


def compressDay(dataframe, minutes, isEvents=False):
    """
    Takes in input dataframe without index containing data
    from a day of the week and
    outputs the grouped version of the dataframe.
    """

    # Select only switch on events because they are certain
    # dayDataframe = dataframe.loc[dataframe['Event'] == 1]
    dayDataframe = dataframe
    # Make column Time as index
    dayDataframe.index = dayDataframe['Time']
    # Create multilevel index by hour and minutes
    if isEvents:
        # YOU WILL PROBABLY NEED TO FIX THIS AS YOU'VE
        # DONE WITH THE NORMAL DATA IN THE ELSE BRANCH.
        grouped = dayDataframe.groupby([dayDataframe.index.hour,
                                        dayDataframe.index.minute//minutes
                                       ]).size()
    else:
        grouped = dayDataframe.groupby([dayDataframe.index.hour,
                                        dayDataframe.index.minute//minutes])

        # You can't calculate the mean per day at a specific hour calling
        # mean() on the grouped object because that object has all
        # the 6 seconds data. Therefore you sum() and then divide by
        # the number of days.
        grouped = grouped.sum()/len(dayDataframe.index.normalize().unique())
    # Rename indexes
    grouped.index.levels[0].name = 'Hour'
    grouped.index.levels[1].name = 'Portion'
    # Convert Series to DataFrame
    groupedDataframe = pd.DataFrame(grouped)
    # groupedDataframe = groupedDataframe.reset_index()
    # groupedDataframe.set_index(['Hour', 'Portion'], inplace=True)
    # Create multi index
    multi_index = pd.MultiIndex.from_product([np.arange(0, 24),
                                              np.arange(0, 60/minutes)],
                                             names=['Hour', 'Portion'])
    # Reindex the dataframe with the newly created multi index
    groupedDataframe = groupedDataframe.reindex(multi_index, fill_value=0)

    return groupedDataframe


def drawCompressedDataframe(dataframe, name, path,
                            isEvents=False, eachDay=True,
                            minutes=60):
    """
    Takes as input a dataframe and a number of minutes
    and draws a plot of a compressed day or for each day of the week.
    """

    # Make the index a normal column
    df = dataframe.reset_index()

    if eachDay:
        # Add a column Day with the name of the day of the week
        # in which an event happened
        df['Day'] = df['Time'].dt.weekday_name
        # For each day of the week
        # Create figure and axes
        fig = plt.figure(figsize=(10, 6))
        ax = fig.add_subplot(111)
        for day in df['Day'].unique():
            # Select the data from the specific day
            dayDataframe = df.loc[df['Day'] == day]
            # compress that data on one day
            groupedDataframe = compressDay(dayDataframe, minutes, isEvents)
            # Create plot bar
            ax = groupedDataframe.plot(kind='bar', ax=ax, position=0)
            # Create legend
            # ax.legend(('Events amount', ))
            # Create title of the plot
            fig.suptitle(name+' '+day)
            # Get the xlabels
            labels = [item.get_text() for item in ax.get_xticklabels()]
            # Prettify labels
            for i, _ in enumerate(labels):
                match = re.search(r'0\.0', labels[i])
                if match:
                    labels[i] = re.sub(r'\((\d+)\W+(\d+\.\d)\)',
                                       r'\1', labels[i])
                else:
                    labels[i] = re.sub(r'\((\d+)\W+(\d+\.\d)\)',
                                       lambda m: str(int(m.groups()[0]))+'.' +
                                       str(int(float(m.groups()[1])*minutes)),
                                       labels[i])
            # Set the new labels
            ax.set_xticklabels(labels)
            # Remove unnecessary legend
            ax.legend_.remove()
            # Force yticks to integer
            ax.yaxis.set_major_locator(MaxNLocator(integer=True))
            # Set axes label
            plt.xlabel('Hour of the day')
            if isEvents:
                plt.ylabel('Number of events')
            elif not isEvents:
                plt.ylabel('Average Watts')
            # Set font and rotation of xticks labels
            plt.xticks(fontsize=6, rotation=45)
            # Show plot
            # plt.show()
            # Save plot to picture
            plt.savefig(path+'/'+name+'_'+day+".svg")
            plt.cla()
        plt.close(fig)
    elif not eachDay:
        # Create figure and axes
        fig = plt.figure(figsize=(10, 6))
        ax = fig.add_subplot(111)
        # compress that data on one day
        groupedDataframe = compressDay(df, minutes, isEvents)
        # Create plot bar
        ax = groupedDataframe.plot(kind='bar', ax=ax, position=0)

        # Get the xlabels
        labels = [item.get_text() for item in ax.get_xticklabels()]
        # Prettify labels
        for i, _ in enumerate(labels):
            match = re.search(r'0\.0', labels[i])
            if match:
                labels[i] = re.sub(r'\((\d+)\W+(\d+\.\d)\)', r'\1', labels[i])
            else:
                labels[i] = re.sub(r'\((\d+)\W+(\d+\.\d)\)',
                                   lambda m: str(int(m.groups()[0])) + '.' +
                                   str(int(float(m.groups()[1])*minutes)),
                                   labels[i])
        # Set the new labels
        ax.set_xticklabels(labels)
        # Remove unnecessary legend
        ax.legend_.remove()
        # Force yticks to integer
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        # Set axes label
        plt.xlabel('Hour of the day')
        if isEvents:
            plt.ylabel('Number of events')
        elif not isEvents:
            plt.ylabel('Average Watts')
        # Set font and rotation of xticks labels
        plt.xticks(np.arange(0, 24.19, 1.0), [i for i in range(24)], rotation=0)
        # plt.xticks(fontsize=6, rotation=45)
        # Save plot to picture
        plt.savefig(path+'/'+name+'_Compressed'+".svg")
        plt.cla()
        plt.close(fig)


def drawCompressedVector(vectors, name, path, isEvents=False):
    """
    Takes as input vector (-1,24) and plot the average watt
    consumption per hour.
    """
    mean = np.mean(vectors, axis=0)
    mean = pd.DataFrame({'Watts': mean})

    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(111)
    ax = mean.plot(kind='bar', ax=ax, position=0)
    # Remove unnecessary legend
    ax.legend_.remove()
    # Force yticks to integer
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    plt.xlabel('Hour of the day')
    if isEvents:
        plt.ylabel('Number of events')
    elif not isEvents:
        plt.ylabel('Average Watts')
    plt.xticks(fontsize=6, rotation=45)
    plt.savefig(path+'/'+name+'_CompressedByHours'+".svg")
    plt.cla()
    plt.close(fig)


def findInterruptions(dataframe, minutes):
    """
    Find interruptions in data longer than minutes.
    """
    # Convert the index of the dataframe in series and get the differences
    # between consecutive data points
    differences = dataframe.index.to_series().diff()
    # Keep only the differences >= duration
    differences = differences[differences >=
                              datetime.timedelta(minutes=minutes)]
    # Get the string of the day of the data points
    # when the data gathering resumed
    toRemove = [datetime.datetime.strftime(date, '%Y-%m-%d')
                for date in differences.index.date]
    # Get the string of the day of the data points
    # when the data gathering was interrupted
    alsoToRemove = differences.index - differences
    alsoToRemove = [datetime.datetime.strftime(date, '%Y-%m-%d')
                    for date in alsoToRemove[0:]]
    # Merge the two lists
    toRemove = np.append(toRemove, alsoToRemove)
    # If the first data point is not in the first hour of
    # the day, delete the first day
    if dataframe.index[0].hour != 0:
        toRemove = np.append(toRemove, datetime.datetime.strftime(
            dataframe.index[0], '%Y-%m-%d'))
    # If the last data point is not in the last hour of
    # the day, delete the last day
    if dataframe.index[-1].hour != 23:
        toRemove = np.append(toRemove, datetime.datetime.strftime(
            dataframe.index[-1], '%Y-%m-%d'))

    # Convert to set to delete duplicates and then convert to list again
    toRemove = list(set(toRemove))

    return toRemove


def removeDays(dataframe, toRemove):
    """
    Remove interruptions found by findInterruptions().
    """
    copiedDataframe = dataframe.copy()
    for day in toRemove:
        copiedDataframe.drop(copiedDataframe[day].index, inplace=True)

    return copiedDataframe


def buildFakeGaussians():
    """
    Build synthetic data with 4 Gaussian probability distributions
    """
    s = np.round_(np.random.normal(1.5, 1.5, 10))
    t = np.round_(np.random.normal(6.5, 1, 30))
    u = np.round_(np.random.normal(19, 1, 40))
    v = np.round_(np.random.normal(22, 1.5, 80))
    train = np.append(s, t)
    train = np.append(train, u)
    train = np.append(train, v)
    train = train.reshape(-1, 1)


def plotBICvsAIC(trainingSet):
    """
    Plot AIC vs BIC against increasing components in the GMM.
    """
    n_components = np.arange(1, 10)
    models = ([mixture.GaussianMixture(n_components=n,
                                       covariance_type='full',
                                       random_state=0).fit(trainingSet)
               for n in n_components])
    plt.figure()
    plt.gca()
    bic = np.array([m.bic(trainingSet) for m in models])
    aic = np.array([m.aic(trainingSet) for m in models])
    # print('Best model BIC: ', bic.argmin()+1)
    # print('Best model AIC: ', bic.argmin()+1)
    plt.plot(n_components, bic, label='BIC')
    plt.plot(n_components, aic, label='AIC')
    plt.legend(loc='best')
    plt.xlabel('n_components')
    # ax.set_ylim([5300, 5900])
    plt.show()


def trainGMM(df):
    """
    Fit GMM to training data.
    """

    # Transform the index of the df in the Time column
    df = df.reset_index()
    # Add a column with the day of the week of an event
    df['Day'] = df['Time'].dt.weekday_name
    # Restore the time index
    df.index = df['Time']

    # # Add constraints on the data
    # df = df.loc[(df['Day'] == 'Monday')]
    # df = df.loc[(df['Day'] == 'Monday') & (df['Event'] == 1)]

    # Transform timestamps in just the hour of the day
    df['Time'] = [timestamp.time() for timestamp in df['Time']]
    # Get the amount of seconds from start of the day: Hour 0
    df['Epoch'] = [(t.hour * 3600) + (t.minute * 60) for t in df['Time']]

    # Split in train/test sets
    # train, test = train_test_split(df['Epoch'], test_size=0.2)
    # train = np.array(train).reshape(-1,1)

    # Reshape the column to work with scikit methods
    train = np.array(df['Epoch']).reshape(-1, 1)

    # Create and fit normal GMM
    # gmm = mixture.GaussianMixture(n_components=2, covariance_type='full')
    # gmm.fit(train)

    # Create and fit Bayesian GMM with Dirichlet process
    gmm = mixture.BayesianGaussianMixture(n_components=10,
                                          covariance_type='full',
                                          max_iter=1500,
                                          weight_concentration_prior=18)
    gmm.fit(train)

    return (gmm, train)


def getParametersOfFittedGMM(gmm):
    """
    Returns the relevant parameters of the GMM fitted to data.
    """


    # # Get all the parameters of the Bayesian GMM
    # parameters = np.array([p * norm.pdf(np.arange(0, 86400), mu, sd)
    #                        for mu, sd, p
    #                        in zip(gmm.means_.flatten(),
    #                        np.sqrt(gmm.covariances_.flatten()),
    #                        gmm.weights_)])

    # Only get the relevant parameters of the Bayesian GMM
    parameters = []
    for mu, sd, p in zip(gmm.means_.flatten(),
                         np.sqrt(gmm.covariances_.flatten()),
                         gmm.weights_):
        if p > 0.01:
            parameters.append(p * norm.pdf(np.arange(0, 86400), mu, sd))

    print("active parameters: %d" % len(parameters))
    parameters = np.array(parameters)

    return parameters


def plotFittedGMM(gmm, parameters, train):
    """
    Plot all components of fitted GMM.
    """

    # Give the same colour to data points of the same Gaussian
    color_list = ['b', 'orange', 'g', 'r', 'k', 'm', 'y', 'brown']
    labels = gmm.predict(train)
    components = list(set(labels))
    colors = [color_list[components.index(label)] for label in labels]

    # # Plot the Training data points
    # y = gmm.score_samples(train)
    y = np.exp(gmm.score_samples(train))
    plt.figure()
    ax = plt.gca()
    ax.scatter(train, y, c=colors)

    # Plot the GMM components
    for i in range(len(components)):
        ax.plot(np.arange(0, 86400), parameters[i], ls='dashed',
                label='Gaussian '+str(i))
    # Set axis limits and ticks
    ax.set_ylim([0, 0.00011])
    locs = np.arange(0, 86399, 20000)
    labs = ([datetime.datetime.utcfromtimestamp(epoch).time().strftime("%H")
             for epoch in locs])
    plt.xticks(locs, labs, rotation=45)
    ax.legend()
    plt.show()


def createJSONExtremes(path):
    """
    Create a json with the first and last date of channel(s).
    """
    with open('Dates.json', 'w') as fp:
        json.dump(getTimeExtremes(path), fp,
                  sort_keys=True, indent=4)


def preprocessingVanillaAutoencoder(dataframe, split=True):
    """
    Process dataframe to make it ready for vanilla autoencoder
    and return the training and testing sets.
    """

    hours = pd.DataFrame()
    # Get the amount of power consumed every hour and
    # Delete all the NaN that are created in the process
    # i.e. when some data points are missing
    hours['Watts'] = dataframe.Watts.resample('H').sum().dropna()

    # For some reason this operation is introducing days with value
    # 0 for each hour. The next block is to remove these extra fake days.
    # Find all different days in original dataframe
    dfDays = dataframe.index.normalize().unique()
    # Get their string representation
    dfDays = [x.strftime("%Y-%m-%d") for x in dfDays]
    # Find all different days in resampled dataframe
    vectorDays = hours.index.normalize().unique()
    # Get their string representation
    vectorDays = [x.strftime("%Y-%m-%d") for x in vectorDays]
    # Pick the difference between the two list of days
    toRemove = list(set(vectorDays).symmetric_difference(set(dfDays)))
    # Remove them from the resampled dataframe
    for day in toRemove:
        hours.drop(hours[day].index, inplace=True)

    # Create two numpy arrays with the values of the index and the Watts
    hoursLabel = hours.index
    hoursLabel = hoursLabel.values
    hoursValue = hours.values

    # Reshape to have 24 columns
    hoursValue = hoursValue.reshape(-1, 24)
    hoursLabel = hoursLabel.reshape(-1, 24)
    hoursValue = hoursValue.astype(np.int)

    if split:
        # Split the arrays in training and test set
        X_train, X_test, Y_train, Y_test = train_test_split(
            hoursValue, hoursLabel,
            test_size=0.15, shuffle=True)
        # Split training set in training and validation set
        # The training set is now 70% of the original set
        X_train, X_val, Y_train, Y_val = train_test_split(
            X_train, Y_train,
            test_size=0.177, shuffle=True)

        return X_train, X_val, X_test, Y_train, Y_val, Y_test
    elif not split:
        return hoursValue, hoursLabel


def drawColorBar(vectors, name, path):
    """
    Takes as input a vector of shape (-1,24) and plots a
    color bar graph.
    """
    vector1 = preprocessing.minmax_scale(vectors, axis=1)
    vector1 = vector1.astype(np.float32)

    fig, ax = plt.subplots()
    im = ax.imshow(vector1, cmap=plt.cm.YlOrRd, interpolation='nearest',
                   aspect='auto', vmin=0, vmax=1)
    fig.colorbar(im)
    plt.yticks([], [])
    plt.savefig(path+'/'+name+'Colorbar1.svg',
                bbox_inches='tight',
                format='svg')

    vector0 = preprocessing.minmax_scale(vectors, axis=0)
    vector0 = vector0.astype(np.float32)

    fig, ax = plt.subplots()
    im = ax.imshow(vector0, cmap=plt.cm.YlOrRd, interpolation='nearest',
                   aspect='auto', vmin=0, vmax=1)
    fig.colorbar(im)
    plt.yticks([], [])
    plt.savefig(path+'/'+name+'Colorbar0.svg',
                bbox_inches='tight',
                format='svg')

    plt.cla()
    plt.close(fig)

    # plt.show()


def findEmptyDays(vectors):
    """
    Takes as input a 2d vector of shape (-1,24) and
    finds positions of all vector containing only zeros.
    """
    return np.where((vectors == np.zeros(24)).all(axis=1))[0]


def removeOffNoise(dataframe, threshold):
    """
    Set to zero data points < threshold.
    This is useful to delete electrical noise.
    """
    copiedDataframe = dataframe.copy()
    copiedDataframe[copiedDataframe.Watts < threshold] = 0
    return copiedDataframe


def findVacations(vectors, consecutiveDays, labels=None):
    """
    Find consecutive days of zero electrical consumption.
    If the label vector is passed as the 'labels' argument,
    return the string representation of the day instead of
    the position in the hourlyVector.
    """
    dailyConsumptions = vectors.sum(axis=1)
    empty = np.where(dailyConsumptions == 0)[0]

    # Groups together consecutive elements.
    ranges = []
    for _, g in groupby(enumerate(empty), lambda x: x[0]-x[1]):
        ranges.append(list(map(itemgetter(1), g)))
    # Create array with all the positions to remove
    # from the original array
    positionToRemove = []
    for days in ranges:
        if len(days) >= consecutiveDays:
            for day in days:
                positionToRemove.append(day)
    # Return the positions
    if labels is None:
        return positionToRemove
    # Return the string representation of the days
    else:
        dayToRemove = []
        for position in positionToRemove:
            date = labels[position][0]
            date = pd.to_datetime(str(date))
            dayToRemove.append(datetime.datetime.strftime(date, '%Y-%m-%d'))

        return dayToRemove


def cleanData(key, dataframe, path,
              interruptionThreshold, offThreshold, vacationThreshold):
    """
    Remove from original data the days before/after an interruption,
    the electrical noise, and the vacation days.
    """
    print('Starting to remove interruptions from {} file.'.format(key))
    toRemove = findInterruptions(dataframe, interruptionThreshold)
    dataframe = removeDays(dataframe, toRemove)
    print('Removed interruptions from {} file.'.format(key))
    dataframe.to_pickle(path+'/'+key+'_NoInterruptions.pickle')
    print('Starting to remove off noise from {} file.'.format(key))
    dataframe = removeOffNoise(dataframe, offThreshold)
    print('Removed off noise from {} file.'.format(key))
    print('Starting to remove vacations from {} file.'.format(key))
    hourlyVectors, hourlyLabels = preprocessingVanillaAutoencoder(dataframe,
                                                                  False)
    toRemove = findVacations(hourlyVectors, vacationThreshold, labels=hourlyLabels)
    dataframe = removeDays(dataframe, toRemove)
    print('Removed vacations from {} file.'.format(key))

    if key == 'kitchen_lights_Cleaned':
        dataframe = dataframe['2013-04-26':]
        print('Deleted old kitchen Lights data.')

    dataframe.to_pickle(path+'/'+key+'_Cleaned.pickle')
    print('Saved cleaned pickle.')

    return dataframe


def drawHistogramDailyConsumption(vectors, path=None, name=None, bins=25):
    """
    Draw histogram of the total daily consumption for each day.
    path and name are used to save the image to disk.
    """
    dailyConsumptions = vectors.sum(axis=1)
    fig, ax = plt.subplots()
    counts, bins, patches = ax.hist(dailyConsumptions, bins=bins,
                                    align='mid', edgecolor="k")
    ax.set_xticks(bins)
    plt.xticks(rotation=45)
    if (path is not None) and (name is not None):
        plt.savefig(path+'/'+name+'_TotalDailyConsumption'+".svg")
        plt.show()
    else:
        plt.show()

    plt.cla()
    plt.close(fig)


def rollVectors(hourlyVectors, append=True):
    """
    Shift input vectors in all 23 positions and return a vector
    of shape (24,-1,24), where in position 0 you have the original,
    position 1 the vectors are shifted of one positions, etc.
    """
    if len(hourlyVectors.shape) < 3:
        vectors = hourlyVectors.copy().reshape(1, -1, 24)
    elif len(hourlyVectors.shape) == 3:
        vectors = hourlyVectors.copy()

    for i in np.arange(1, 24):
        vectors = np.append(vectors,
                            np.roll(hourlyVectors, i, axis=1).reshape(1, -1, 24),
                            axis=0)

    if append:
        return vectors
    else:
        toRemove = []
        for group in vectors:
            toRemove.append(np.array_equal(group, hourlyVectors))
        toRemove = np.array(toRemove)
        toRemove = np.where(toRemove)
        return np.delete(vectors, toRemove, axis=0)

def scaleVectors(hourlyVectors, scales, append=False):
    """
    Scale input vectors by all the scales given as input.
    Can also give as input the result of rollVectors().
    """
    if len(hourlyVectors.shape) < 3:
        vectors = hourlyVectors.copy().reshape(1, -1, 24)
        needSubscript = False
    elif len(hourlyVectors.shape) == 3:
        vectors = hourlyVectors.copy()
        needSubscript = True

    rows = vectors.shape[1]
    for scale in scales:
        vectors = np.append(vectors, (hourlyVectors).reshape(-1, rows, 24)*scale, axis=0)

    if append:
        return vectors
    else:
        toRemove = []
        if needSubscript:
            for i, _ in enumerate(hourlyVectors):
                toRemove.append(np.array_equal(vectors[i], hourlyVectors[i]))
        else:
            for group in vectors:
                toRemove.append(np.array_equal(group, hourlyVectors))
        toRemove = np.array(toRemove)
        toRemove = np.where(toRemove)

        return np.delete(vectors, toRemove, axis=0)

def flattenConsumption(hourlyVectors, append=False):
    """
    Takes in input the standard output of preprocessingVanillaAutoencoder.
    For each input vector, it returns vectors with inside only
    the maximum, minimum, and the average. The returned vector
    has shape (3,-1,24).
    """
    vectors = hourlyVectors.copy()

    values = [vectors.max(axis=1), vectors.min(axis=1), vectors.mean(axis=1)]

    vectors = vectors.reshape(1, -1, 24)
    for i, _ in enumerate(values):
        vectors = np.append(vectors, changeMatrixRows(hourlyVectors, values[i]), axis=0)

    if append:
        return vectors
    else:
        toRemove = []
        for group in vectors:
            toRemove.append(np.array_equal(group, hourlyVectors))
        toRemove = np.array(toRemove)
        toRemove = np.where(toRemove)
        return np.delete(vectors, toRemove, axis=0)


def changeMatrixRows(hourlyVectors, values):
    """
    Substitute each matrix's row with a given value.
    """
    vectors = hourlyVectors.copy()

    for position, item in enumerate(values):
        vectors[position] = item

    return vectors.reshape(1, -1, 24)

def generateAnomalies(hourlyVectors, scales=[0.5, 0.75, 1.25, 1.5, 2]):
    """
    Generate all anomalies. If cumulate is True, also scale the rolled anomalies.
    """
    vectors = hourlyVectors.copy()

    rolled = rollVectors(vectors)
    scaled = scaleVectors(vectors, scales)
    rolledAndScaled = scaleVectors(rollVectors(vectors), scales)
    flattened = flattenConsumption(vectors)

    return rolled, scaled, rolledAndScaled, flattened

def visualizeDistanceVectors(hourlyVectors, path=None):
    """
    Draw three figures to understand how input vectors
    relate to themselves, if path is set, it saves the
    images to that path.
    """

    vectors = hourlyVectors.copy()
    distances = euclidean_distances(vectors, vectors)

    fig, ax = plt.subplots()
    plt.hist(distances.reshape(1399*1399), bins=50, edgecolor='k')
    locs, labels = plt.yticks()
    ax.set_yticklabels(np.array(locs/2).astype(int))
    if path:
        plt.savefig(path+'/Distance_Histogram.svg')
    else:
        plt.show()
    plt.clf()

    sns.heatmap(distances, vmin=distances.min(), vmax=distances.max(),
                cmap='seismic', center=distances.mean())
    if path:
        plt.savefig(path+'/Distance_Heatmap.png', dpi=300)
    else:
        plt.show()
    plt.clf()

    mds = manifold.MDS(n_components=2, max_iter=300, eps=1e-3, random_state=4,
                       dissimilarity="precomputed", n_jobs=1)
    pos = mds.fit(distances).embedding_
    plt.scatter(pos[:, 0], pos[:, 1], color='navy', lw=0, label='MDS')
    if path:
        plt.savefig(path+'/Distance_Manifold.png', dpi=300)
    else:
        plt.show()
    plt.clf()
    plt.cla()
    plt.close(fig)


# The method below is obsolete because its functionalities 
# were split and included in the autoencoder class

# def trainVanillaAutoencoder(dataframe, encodingDim, activations, scaler, loss):
#     from keras.layers import Input, Dense
#     from keras.models import Model
#     from keras import callbacks
#     # this is our input placeholder
#     inputVector = Input(shape=(24,))
#     # "encoded" is the encoded representation of the input
#     encoded = Dense(encodingDim, activation=activations[0])(inputVector)
#     # "decoded" is the lossy reconstruction of the input
#     decoded = Dense(24, activation=activations[1])(encoded)

#     # this model maps an input to its reconstruction
#     autoencoder = Model(inputVector, decoded)

#     X_train, X_val, X_test, Y_train, Y_val, Y_test = \
#         preprocessingVanillaAutoencoder(dataframe, split=True)
#     X_train_original = X_train.copy()
#     X_test_original = X_test.copy()
#     X_val_original = X_val.copy()

#     scaler_loc = scaler[1]
#     if scaler_loc is not None:
#         X_train = scaler_loc.fit_transform(X_train)
#         X_train = X_train.astype(np.float32)
#         X_val = scaler_loc.transform(X_val)
#         X_val = X_val.astype(np.float32)
#         X_test = scaler_loc.transform(X_test)
#         X_test = X_test.astype(np.float32)

#     # Compile model
#     epochs = 1500
#     # learning_rate = 0.1
#     # decay_rate = learning_rate / epochs
#     # momentum = 0.8
#     # sgd = keras.optimizers.SGD(lr=learning_rate, momentum=momentum,
#                                # decay=decay_rate, nesterov=True)
#     now = time()
#     myTensorBoard = callbacks.TensorBoard(log_dir='./TensorGraph/{}'.format(now), histogram_freq=10,
#                                           write_graph=True, write_images=True, write_grads=True)
#     myEarlyStopping = callbacks.EarlyStopping(monitor='val_loss', min_delta=0,
#                                               patience=10, verbose=2, mode='auto')
#     myCallbacks = [myTensorBoard, myEarlyStopping]
#     autoencoder.compile(optimizer='adam', loss=loss[1])

#     try:
#         history = autoencoder.fit(X_train, X_train,
#                                   epochs=epochs,
#                                   batch_size=64,
#                                   shuffle=True,
#                                   validation_data=(X_val, X_val),
#                                   callbacks=myCallbacks,
#                                   verbose=0)
#     except:
#         msg = """ERROR! This run used data scaled with {},
#                 encoding dimension {},
#                 activations {}, loss {}. 
#                 The time was {}\n""".format(scaler[0],
#                                             encodingDim,
#                                             activations,
#                                             loss[0],
#                                             now)
#         return -1, msg

#     X_train_reconstructed = autoencoder.predict(X_train, batch_size=1)
#     X_val_reconstructed = autoencoder.predict(X_val, batch_size=1)
#     if scaler[1] is not None:
#         X_train_reconstructed = scaler[1].inverse_transform(X_train_reconstructed)
#         X_val_reconstructed = scaler[1].inverse_transform(X_val_reconstructed)
#     mae_train = mean_absolute_error(X_train_original, X_train_reconstructed)
#     mae_val = mean_absolute_error(X_val_original, X_val_reconstructed)

#     if scaler[1] is not None:
#         message = """This run used data scaled with {},
#                 encoding dimension {},
#                 activations {}, loss {}, 
#                 and achieved MAE on the training set {}
#                 and on the validation set {}. The time was {}\n""".format(scaler[0],
#                                                                           encodingDim,
#                                                                           activations,
#                                                                           loss[0],
#                                                                           mae_train,
#                                                                           mae_val,
#                                                                           now)
#     else:
#         message = """This run used NON SCALED data,
#                 encoding dimension {},
#                 activations {}, loss {}, 
#                 and achieved MAE on the training set {},
#                 and on the validation set {}. The time was {}\n""".format(encodingDim,
#                                                                           activations,
#                                                                           loss[0],
#                                                                           mae_train,
#                                                                           mae_val,
#                                                                           now)
#     # For hyperparameter search
#     # return mae_train, message

#     # For evaluating
#     return autoencoder, X_train_original, X_val_original, X_test_original, [scaler[0], scaler_loc]

def load_saved_model(path, name):
    """
    Returns all attributes of a saved model.
    """
    from keras.models import load_model
    
    model = load_model(path+'/'+name+'.hdf5')

    with open(path+'/'+name+'.pickle', 'rb') as handle:
        attributes = pickle.load(handle)

    attributes['model'] = model

    return attributes



def mean_absolute_deviation_features(original, reconstructed, single_vector=False):
    """
    Calculate Mean Absolute Deviation of the Errors (MADE) 
    for a single day or a list of days.
    """
    if single_vector:
        abs_errors = np.absolute(np.subtract(original, reconstructed))
        gamma = np.mean(np.absolute(abs_errors - mean_absolute_error(original, reconstructed)))
        return gamma
    else:
        madf = []
        for i,_ in enumerate(original):
            abs_errors = np.absolute(np.subtract(original[i], reconstructed[i]))
            gamma = np.mean(np.absolute(abs_errors - mean_absolute_error(original[i], reconstructed[i])))
            madf.append(gamma)
        return np.array(madf)




