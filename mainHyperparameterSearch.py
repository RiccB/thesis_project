"""
This module uses the autoencoder module to show how to 
train an autoencoder and do a small hyperparameter search.
"""

import autoencoder
import dataMunging
import numpy as np
import os
import pandas as pd
import pickle
import random

from multiprocessing import Manager
from multiprocessing import Process
from sklearn import preprocessing


def main():
    """
    Perform a small hyperparameter search for a
    single-layer autoencoder.
    """

    # Get the data from disk location
    with open('Models/Microwave/Microwave_data_sets.pickle', 'rb') as h:
        x_train, x_val, x_test = pickle.load(h)
    # Set all the hyperparameters to explore
    # encoding_dims = [[24, 12], [24, 8], [24, 6], [24, 4]]
    encoding_dims = [[24, 12], [24, 8], [24, 6], [24, 4]]
    activations = [['relu', 'relu'],
                   ['relu', 'linear']]
    # activations = [['relu', 'relu'], ['relu', 'linear']]
    scalers = [preprocessing.StandardScaler(), preprocessing.MinMaxScaler(), None]
    losses = ['mse', 'mae', root_mean_squared_error]
    optimizers = ['adam']
    # Create an array with each combination
    args = [[ed, act, s, l, opt] for ed in encoding_dims for act in activations
            for s in scalers for l in losses for opt in optimizers]
    # Set the optimizer for the training
    

    train_models(args, x_train, x_val, x_test, iterations=3)


def train_models(args, x_train, x_val, x_test, iterations):
    # The results array contains the model summary for each combination of hyperparameters
    results = []
    # It is necessary to run Keras inside different processes if somebody wants
    # to train different models in the same file. The following block of code does that.
    for instance_of_args in args:
        manager = Manager()
        # Initialize shared memory dictionary
        return_dict = manager.dict()
        # Set the hyperparameters for this run
        current_args = tuple(instance_of_args) + (x_train, x_val, x_test, return_dict,)
        jobs = []
        # Choose the best result out of 3 tests with the same hyperparameters
        for i in range(iterations):
            # Set the seed, otherwise every process will inherit
            # the seed from the parent process, and therefore
            # every run will yield the same results.
            seed = random.randrange(4294967295)
            np.random.seed(seed=seed)

            training_process = Process(target=_training_worker, args=current_args)
            jobs.append(training_process)
            training_process.start()

        for proc in jobs:
            proc.join()

        best_result = return_dict[min(list(return_dict.keys()))]
        results.append(best_result)
        # Since the training of all the possible models takes a lot of time,
        # save to a file the results obtained until now
        with open(os.getcwd()+'/MicrowaveVanillaCurrentResults.txt', 'a') as text_file:
            text_file.write("%s\n" % best_result)

    # Save to a file all the results obtained
    with open(os.getcwd()+'/MicrowaveVanillaFinalResults.txt', 'w') as text_file:
        for item in results:
            text_file.write("%s\n" % item)



def build_autoencoder(layers_dim, activation_funcs, scaler, loss,
                      optimizer, x_train, x_val, x_test):
    """
    Builds and fits a single-layer autoencoder with the passed arguments.
    """
    
    if len(layers_dim) == 2:
        print('Building vanilla autoencoder.')
        ae = autoencoder.VanillaAutoencoder(layers_dim, activation_funcs)
        ae.compile(loss, optimizer)
        ae.set_data(x_train, x_val, x_test, scaler)
        ae.fit()
    elif len(layers_dim) == 4:
        print('Building multi-layer autoencoder.')
        ae = autoencoder.SevenLayerAutoencoder(layers_dim, activation_funcs)
        ae.compile(loss, optimizer)
        ae.set_data(x_train, x_val, x_test, scaler)        
        # ae.fit_layer_wise()  
        ae.fit()  

    return ae

def _training_worker(layers_dim, activation_funcs, scaler, loss,
                     optimizer, x_train, x_val, x_test, returnDict):
    """
    This code runs in a separate process.
    """
    ae = build_autoencoder(layers_dim, activation_funcs, scaler, loss, 
                           optimizer, x_train, x_val, x_test)
    returnDict[ae.test_mae] = ae.msg

def root_mean_squared_error(y_true, y_pred):
    """
    Alternative loss functions that penalizes big differences in values.
    """
    # Import here to avoid Keras errors when training multiple models in single script.
    from keras import backend as K
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))

if __name__ == '__main__':
    main()