"""
The autoencoder module provides the class Vanilla autoencoder,
and other functions that support the creations of single-layer
autoencoder objects.
"""

import numpy as np
import os
import pickle
import re

from sklearn.metrics import mean_absolute_error
from time import time


class VanillaAutoencoder:
    """
    Defines a single-layer autoencoder and its methods.
    """
    def __init__(self, layers_dim, activation_funcs):
        """
        Initializes all instance values.
        """

        from keras.layers import Dense
        from keras.layers import Input
        from keras.models import Model
        from keras import regularizers

        # Arguments layers_dim, activation_funcs
        # must be arrays of dimension 2

        # Set layer dimensions
        self.layers_dim = {'input': layers_dim[0],
                           'bottleneck': layers_dim[1],
                           'output': layers_dim[0]}
        # Set layer activation function
        self.activation_funcs = {'encoder': activation_funcs[0],
                                 'decoder': activation_funcs[1]}

        # Input placeholder
        input_vector = Input(shape=(self.layers_dim['input'],))
        # Bottleneck layer
        encoded = Dense(self.layers_dim['bottleneck'],
                        activation=self.activation_funcs['encoder'],
                        activity_regularizer=regularizers.l1(10e-5))(input_vector)
        # Output layer
        decoded = Dense(self.layers_dim['output'],
                        activation=self.activation_funcs['decoder'])(encoded)
        # Initialize model
        self.model = Model(input_vector, decoded)
        # Initialize all remaining instance attributes for legibility
        self.loss = None
        self.scaler = None
        self.optimizer = None
        self.x_train = None
        self.x_val = None
        self.x_test = None
        # The 'original' sets are useful for when data are scaled
        self.x_train_original = None
        self.x_val_original = None
        self.x_test_original = None
        # Mean absolute error over the test set
        self.test_mae = None
        # Mean absolute error over the validation set
        self.val_mae = None
        # String summary of the model.
        self.msg = None

    def save_attributes(self, name):
        variables = {'layers_dim': self.layers_dim,
                     'activation_funcs': self.activation_funcs,
                     'loss': self.loss,   
                     'scaler': self.scaler,
                     'optimizer': self.optimizer,
                     'x_train': self.x_train,
                     'x_val': self.x_val,
                     'x_test': self.x_test,
                     'test_mae': self.test_mae,
                     'val_mae': self.val_mae,
                     'msg': self.msg}
        with open(name+'.pickle', 'wb') as handle:
            pickle.dump(variables, handle)

        self.model.save(name+'.hdf5')       


    def compile(self, loss, optimizer):
        """
        Compile the model with chosen loss function and optimizer.
        """
        self.loss = loss
        self.optimizer = optimizer
        self.model.compile(optimizer=optimizer, loss=loss)


    def set_data(self, x_train, x_val, x_test, scaler=None):
        """
        Set the data sets to train, validate and test the model.
        If a scaler is passed, scale the data.
        """
        self.x_train_original = x_train.copy()
        self.x_val_original = x_val.copy()
        self.x_test_original = x_test.copy()

        if scaler is None:
            self.x_train = x_train.copy()
            self.x_val = x_val.copy()
            self.x_test = x_test.copy()
            self.scaler = None
        else:
            args = (x_train.copy(), x_val.copy(), x_test.copy(), scaler)
            (self.x_train, self.x_val, self.x_test,
             self.scaler) = scale_data(*args)


    def fit(self,):
        """
        Fit the model to the data and compute the
        Mean Absolute Error on the test data.
        """
        import keras
        from keras import callbacks
        from tensorflow.python.framework.errors_impl import InvalidArgumentError


        # Each training of the model is recorded in TensorBoard format
        # and saved in a folder with the initial time of training
        now = time()
        tensorgraph_path = os.path.join(os.getcwd(),
                                        'TensorGraph/{}'.format(now))
        my_tensor_board = callbacks.TensorBoard(log_dir=tensorgraph_path,
                                                histogram_freq=10,
                                                write_graph=True,
                                                write_images=True,
                                                write_grads=True)
        # Set conditions for early stopping of training
        my_early_stopping = callbacks.EarlyStopping(monitor='val_loss',
                                                    min_delta=0,
                                                    patience=10,
                                                    verbose=2,
                                                    mode='auto')
        # Prepare callbacks to be fed to fit method
        my_callbacks = [my_tensor_board, my_early_stopping]
        # Set maximum number of epochs if early stopping is not triggered
        epochs = 1500

        try:
            # Fit model to training data
            self.model.fit(self.x_train, self.x_train,
                           epochs=epochs,
                           batch_size=64,
                           shuffle=True,
                           validation_data=(self.x_val, self.x_val),
                           callbacks=my_callbacks,
                           verbose=0)
            # Compute Mean Absolute Error on test data
            self.compute_val_test_mae()
            # Get the string representation of the scaler
            scaler_str = scaler_tostring(self.scaler)
            # Set model string summary
            self.msg = """This run used data scaled with {},
                    encoding dimension {},
                    activations {}, loss {},
                    and achieved MAE on the validation set {}
                    The time was {}\n""".format(scaler_str,
                                                sorted(self.layers_dim.values()),
                                                self.activation_funcs.values(),
                                                self.loss,
                                                self.val_mae,
                                                now)
        except InvalidArgumentError:
            # Set test and val maes to indicate that an error occurred
            self.test_mae = -1
            self.val_mae = -1
            # Get the string representation of the scaler
            scaler_str = scaler_tostring(self.scaler)
            # Set model string summary
            self.msg = """ERROR, the gradient exploded!
                        This run used data scaled with {},
                        encoding dimension {},
                        activations {}, loss {}.
                        The time was {}\n""".format(scaler_str,
                                                    sorted(self.layers_dim.values()),
                                                    self.activation_funcs.values(),
                                                    self.loss,
                                                    now)
            keras.backend.clear_session()

    def compute_val_test_mae(self):
        """
        Sets the test_mae for the current model,
        using the dataset saved in the instance attributes.
        """
        # Reconstruct the test using the generated model
        x_test_reconstructed = self.model.predict(self.x_test, batch_size=1)
        # If needed, rescale the reconstructed data to original format
        if self.scaler is not None:
            x_test_reconstructed = self.scaler.inverse_transform(
                x_test_reconstructed)
        # Set test_mae
        self.test_mae = mean_absolute_error(
            self.x_test_original, x_test_reconstructed)

        # Reconstruct the validation set using the generated model
        x_val_reconstructed = self.model.predict(self.x_val, batch_size=1)
        # If needed, rescale the reconstructed data to original format
        if self.scaler is not None:
            x_val_reconstructed = self.scaler.inverse_transform(
                x_val_reconstructed)
        # Set val_mae
        self.val_mae = mean_absolute_error(
            self.x_val_original, x_val_reconstructed)




class SevenLayerAutoencoder(VanillaAutoencoder):
    def __init__(self, layers_dim, activation_funcs):
        """
        Initializes all instance values.
        """

        from keras.layers import Dense
        from keras.layers import Input
        from keras.models import Model

        # Argument layers_dim must be an array of dimension 4
        # Argument activation_funcs must be an array of dimension 6

        # Set layer dimensions
        self.layers_dim = {'input': layers_dim[0],
                           'layer1': layers_dim[1],
                           'layer2': layers_dim[2],
                           'bottleneck': layers_dim[3],
                           'layer4': layers_dim[2],
                           'layer5': layers_dim[1],
                           'output': layers_dim[0]}
        # Set layer activation function
        self.activation_funcs = {'layer1': activation_funcs[0],
                                 'layer2': activation_funcs[1],
                                 'bottleneck': activation_funcs[2],
                                 'layer4': activation_funcs[3],
                                 'layer5': activation_funcs[4],
                                 'output': activation_funcs[5]}

        # Input placeholder
        input_vector = Input(shape=(self.layers_dim['input'],))

        layer1 = Dense(self.layers_dim['layer1'],
                       activation=self.activation_funcs['layer1'])(input_vector)
        layer2 = Dense(self.layers_dim['layer2'],
                       activation=self.activation_funcs['layer2'])(layer1)
        bottleneck = Dense(self.layers_dim['bottleneck'],
                           activation=self.activation_funcs['bottleneck'])(layer2)
        layer4 = Dense(self.layers_dim['layer4'],
                       activation=self.activation_funcs['layer4'])(bottleneck)
        layer5 = Dense(self.layers_dim['layer5'],
                       activation=self.activation_funcs['layer5'])(layer4)
        output = Dense(self.layers_dim['output'],
                       activation=self.activation_funcs['output'])(layer5)
        # Initialize model
        self.model = Model(input_vector, output)
        # Initialize all remaining instance attributes for legibility
        self.loss = None
        self.scaler = None
        self.x_train = None
        self.x_val = None
        self.x_test = None
        # The 'original' sets are useful for when data are scaled
        self.x_train_original = None
        self.x_val_original = None
        self.x_test_original = None
        # Mean absolute error over the test set
        self.test_mae = None
        # Mean absolute error over the validation set
        self.val_mae = None
        # String summary of the model.
        self.msg = None

    def fit_layer_wise(self):
        from keras.layers import Dense
        from keras.layers import Input
        from keras.models import Model
        from keras import callbacks
        from tensorflow.python.framework.errors_impl import InvalidArgumentError


        now = time()

        input1 = Input(shape=(self.layers_dim['input'], ))
        encoded1 = Dense(self.layers_dim['layer1'], activation=self.activation_funcs['layer1'])(input1)
        decoded1 = Dense(self.layers_dim['input'], activation = 'linear')(encoded1)
        autoencoder1 = Model(input1, decoded1)
        encoder1 = Model(input1, encoded1)

        input2 = Input(shape=(self.layers_dim['layer1'],))
        encoded2 = Dense(self.layers_dim['layer2'], activation=self.activation_funcs['layer2'])(input2)
        decoded2 = Dense(self.layers_dim['layer1'], activation='linear')(encoded2)
        autoencoder2 = Model(input2, decoded2)
        encoder2 = Model(input2, encoded2)

        input3 = Input(shape=(self.layers_dim['layer2'],))
        encoded3 = Dense(self.layers_dim['bottleneck'], activation=self.activation_funcs['bottleneck'])(input3)
        decoded3 = Dense(self.layers_dim['layer2'], activation = 'linear')(encoded3)        
        autoencoder3 = Model(input3, decoded3)

        autoencoder1.compile(loss=self.loss, optimizer = self.optimizer)
        autoencoder2.compile(loss=self.loss, optimizer = self.optimizer)
        autoencoder3.compile(loss=self.loss, optimizer = self.optimizer)

        # Set conditions for early stopping of training
        my_early_stopping = callbacks.EarlyStopping(monitor='val_loss',
                                                    min_delta=0,
                                                    patience=10,
                                                    verbose=2,
                                                    mode='auto')
        # Prepare callbacks to be fed to fit method
        my_callbacks = [my_early_stopping]
        # Set maximum number of epochs if early stopping is not triggered
        epochs = 1500
        try:
            autoencoder1.fit(self.x_train, self.x_train,
                             epochs=epochs,
                             batch_size=64,
                             shuffle=True,
                             validation_data=(self.x_val, self.x_val),
                             callbacks=my_callbacks,
                             verbose=0)
            first_layer_train = encoder1.predict(self.x_train)
            first_layer_val = encoder1.predict(self.x_val)
            print('Trained first layer.')

            autoencoder2.fit(first_layer_train, first_layer_train,
                             epochs=epochs,
                             batch_size=64,
                             shuffle=True,
                             validation_data=(first_layer_val, first_layer_val),
                             callbacks=my_callbacks,
                             verbose=0)
            second_layer_train = encoder2.predict(first_layer_train)
            second_layer_val = encoder2.predict(first_layer_val)
            print('Trained second layer.')

            autoencoder3.fit(second_layer_train, second_layer_train,
                             epochs=epochs,
                             batch_size=64,
                             shuffle=True,
                             validation_data=(second_layer_val, second_layer_val),
                             callbacks=my_callbacks,
                             verbose=0)
            print('Trained third layer.')

            self.model.layers[1].set_weights(autoencoder1.layers[1].get_weights())
            self.model.layers[2].set_weights(autoencoder2.layers[1].get_weights())
            self.model.layers[3].set_weights(autoencoder3.layers[1].get_weights())
            self.model.layers[4].set_weights(autoencoder3.layers[2].get_weights())
            self.model.layers[5].set_weights(autoencoder2.layers[2].get_weights())
            self.model.layers[6].set_weights(autoencoder1.layers[2].get_weights())

            tensorgraph_path = os.path.join(os.getcwd(),
                                        'TensorGraph/{}'.format(now))
            my_tensor_board = callbacks.TensorBoard(log_dir=tensorgraph_path,
                                                    histogram_freq=10,
                                                    write_graph=True,
                                                    write_images=True,
                                                    write_grads=True)
            # Set conditions for early stopping of training
            my_early_stopping = callbacks.EarlyStopping(monitor='val_loss',
                                                        min_delta=0,
                                                        patience=10,
                                                        verbose=2,
                                                        mode='auto')
            # Prepare callbacks to be fed to fit method
            my_callbacks = [my_tensor_board, my_early_stopping]
            # Fit model to training data
            self.model.fit(self.x_train, self.x_train,
                           epochs=epochs,
                           batch_size=64,
                           shuffle=True,
                           validation_data=(self.x_val, self.x_val),
                           callbacks=my_callbacks,
                           verbose=0)
            print('Fine-tuning whole network.')
            # Compute Mean Absolute Error on test data
            self.compute_val_test_mae()
            # Set model string summary
            self.msg = """This run used data scaled with {},
                    encoding dimension {},
                    activations {}, loss {},
                    and achieved MAE on the validation set {}
                    The time was {}\n""".format(scaler_tostring(self.scaler),
                                                sorted(self.layers_dim.values()),
                                                self.activation_funcs.values(),
                                                self.loss,
                                                self.val_mae,
                                                now)
        except InvalidArgumentError:
            # Set test and val maes to indicate that an error occurred
            self.test_mae = -1
            self.val_mae = -1
            # Get the string representation of the scaler
            scaler_str = scaler_tostring(self.scaler)
            self.msg = """ERROR, the gradient exploded!
                        This run used data scaled with {},
                        encoding dimension {},
                        activations {}, loss {}.
                        The time was {}\n""".format(scaler_str,
                                                    sorted(self.layers_dim.values()),
                                                    self.activation_funcs.values(),
                                                    self.loss,
                                                    now)


def scaler_tostring(scaler):
    """
    Get the string representation of a scaler object.
    """
    if scaler is None:
        return 'None'
    else:
        string = str(scaler.__class__)
        string = re.search(r'\.(\w+)\'', string).group(1)
        return string


def scale_data(x_train, x_val, x_test, scaler):
    """
    Scale data according to the scaler given as argument.
    """
    scaler_loc = scaler
    x_train = scaler_loc.fit_transform(x_train)
    x_train = x_train.astype(np.float32)
    x_val = scaler_loc.transform(x_val)
    x_val = x_val.astype(np.float32)
    x_test = scaler_loc.transform(x_test)
    x_test = x_test.astype(np.float32)

    return x_train, x_val, x_test, scaler_loc

def root_mean_squared_error(y_true, y_pred):
    """
    Alternative loss functions that penalizes big differences in values.
    """
    # Import here to avoid Keras errors when training multiple models in single script.
    from keras import backend as K
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))
