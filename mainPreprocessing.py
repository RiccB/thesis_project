"""
This script performs the preprocessing steps.
"""


import dataMunging
import os
import datetime
import numpy as np
from sklearn import preprocessing


def main():
    # Set the data folder
    path = os.getcwd()+'/Data'

    # # Convert dat file(s) in the path to CSV
    # dataMunging.datToCSV(path)

    # Save csv file(s) as pickle so maintain format change
    print('Starting csv to pickle conversion.')
    dataMunging.csvToPickle(path)
    print('Finished csv to pickle conversion.')

    # # Read the pickle file(s) and put them in a dictionary of structure
    # # {'NameOfPickle': contentOfPickle}
    wattDataFrame = dataMunging.readPickle(path)

    for key, dataframe in wattDataFrame.items():
    #     # # Take part of the dataframe or truncate its length
    #     # wattDataFrame[key] = wattDataFrame[key][datetime(2013, 3, 13):
    #     #                                         datetime(2013, 4, 13)]
    #     # # wattDataFrame[key] = wattDataFrame[key].truncate(
    #     # #                          after='2012-12-31'

        dataframe = dataMunging.cleanData(key=key,
                                          dataframe=dataframe,
                                          path=path,
                                          interruptionThreshold=60,
                                          offThreshold=10,
                                          vacationThreshold=3)

        print('Starting to draw {} plots.'.format(key))
        dataMunging.drawCompressedDataframe(dataframe, name=key, path=os.getcwd()+'/OldData/'+key.replace('_Cleaned', ''), eachDay=False)
        dataMunging.drawCompressedDataframe(dataframe, name=key, path=os.getcwd()+'/OldData/'+key.replace('_Cleaned', ''), eachDay=True)
        hourlyVectors, hourlyLabels = dataMunging.preprocessingVanillaAutoencoder(dataframe, False)
        dataMunging.drawColorBar(hourlyVectors, name=key, path=os.getcwd()+'/OldData/'+key.replace('_Cleaned', ''))
        np.savetxt(os.getcwd()+'/OldData/'+key.replace('_Cleaned','')+'/'+key+' ratio.txt', [len(dataMunging.findEmptyDays(hourlyVectors))/len(hourlyVectors)], fmt='%1.3f')
        #print(np.where((hourlyVectors == np.zeros(24)).all(axis=1))[0])
        # print(len(dataMunging.findEmptyDays(hourlyVectors)), len(hourlyVectors))
        print('Drew all {} plots.'.format(key))
        
        # valueTrain, valueTest, labelTrain, labelTest = dataMunging.preprocessingVanillaAutoencoder(dataframe)
        # dataMunging.drawColorBar(valueTrain, key)


        

    # ##################   OPERATIONS ON EVENTS   ################## #

    # Set the event folders
    path = os.getcwd()+'/Events'
    # Convert dat file(s) in the path to CSV
    # dataMunging.datToCSV(path)
    # # Convert csv file(s) in the path to pickle
    # dataMunging.csvEventToPickle(path)
    # # Read the pickle file(s) and put them in a dictionary
    # # {'NameOfPickle': contentOfPickle}
    # eventDataFrame = dataMunging.readPickle(path)
    # # For each of the dataframes in the dictionary
    # for key, df in eventDataFrame.items():
    #     # # Take part of the dataframe or truncate its length
    #     # eventDataFrame[key] = eventDataFrame[key][datetime(2013, 3, 13):
    #     #                                           datetime(2013, 4, 13)]
    #     # # eventDataFrame[key] = wattDataFrame[key].truncate(
    #     # #                          after='2012-12-31')

    #     # Operations on GMM
    #     gmm, train = dataMunging.trainGMM(df)
    #     parameters = dataMunging.getParametersOfFittedGMM(gmm)
    #     dataMunging.plotFittedGMM(gmm, parameters, train)

    # # Copy the content of the second dictionary after the first one
    # wattDataFrame.update(eventDataFrame)
    # # Plot watts against events
    # dataMunging.drawWattAgainstEvent(wattDataFrame)


if __name__ == '__main__':
    main()
