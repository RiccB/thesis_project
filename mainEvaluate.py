"""
The code in this file was used mainly with the Python interactive
shell to evaluate the results. So it doesn't really make sense to
execute it all in one go.
"""

import os
import numpy as np
import pandas as pd
import sys
# Add current directory to path for sublimerepl
sys.path.append(os.getcwd()) 
import dataMunging
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt
import pickle


def getAnomalyMaes(original, reconstructed):
    """
    Compute MAE errors for a specific kind of anomaly
    """
    maes_perday = [[] for i in range(len(reconstructed))]
    for block in range(len(reconstructed)):
        for day in range(len(reconstructed[block])):
            maes_perday[block].append(mean_absolute_error(original[block][day], reconstructed[block][day]))
    maes_perday = np.array(maes_perday)
    maes_perblock = np.array([mean_absolute_error(original[block], reconstructed[block]) for block in range(len(reconstructed))])

    return maes_perday, maes_perblock

def getAnomalyMads(original, reconstructed):
    """
    Compute MADE errors for a specific kind of anomaly
    """
    mads_perday = [[] for i in range(len(reconstructed))]
    for block in range(len(reconstructed)):
        for day in range(len(reconstructed[block])):
            mads_perday[block].append(dataMunging.mean_absolute_deviation_features(original[block][day], reconstructed[block][day], True))
    mads_perday = np.array(mads_perday)
    return mads_perday


def reconstructAnomalies(anomalies, autoencoder):
    """
    Reconstruct a specific kind of anomaly
    """
    reconstructed = np.empty((0, anomalies.shape[1], anomalies.shape[2]))
    for block in anomalies:
        reconstructed = np.append(reconstructed, autoencoder.predict(block, batch_size=1).reshape(1, anomalies.shape[1], anomalies.shape[2]), axis=0)

    return reconstructed

def save_anomalies_results(path, name):
    """
    Save all the relevant data regarding the anomalies for a model in
    a pickle.
    """
    model_attr = dataMunging.load_saved_model(path,name)
    x_test = model_attr['x_test']
    x_train = model_attr['x_train']
    model = model_attr['model']

    x_train_reconstructed = model.predict(x_train, batch_size=1)
    x_test_reconstructed = model.predict(x_test, batch_size=1)

    # Define the reconstruction error range of the training set
    train_mae = mean_absolute_error(x_train, x_train_reconstructed)
    train_maes = []
    for index, _ in enumerate(x_train):
        train_maes.append(mean_absolute_error(x_train[index], x_train_reconstructed[index]))
    train_maes = np.array(train_maes)
    threshold_mae = np.percentile(train_maes, 95)
    train_interval = (min(train_maes), max(train_maes))


    # Define MAD train threshold
    train_mads = dataMunging.mean_absolute_deviation_features(x_train, x_train_reconstructed)
    threshold_mad = np.percentile(train_mads, 95)


    # Define the reconstruction error range of the test set
    test_mae = mean_absolute_error(x_test, x_test_reconstructed)
    test_maes = []
    for index, _ in enumerate(x_test):
        test_maes.append(mean_absolute_error(x_test[index], x_test_reconstructed[index]))
    test_maes = np.array(test_maes)
    test_interval = (min(test_maes), max(test_maes))

    # Define MAD test threshold
    test_mads = dataMunging.mean_absolute_deviation_features(x_test, x_test_reconstructed)

    # Generate the anomalies
    rolled, scaled, rolledAndScaled, flattened = dataMunging.generateAnomalies(x_test)

    # Reconstruct anomalies
    rolled_reconstructed = reconstructAnomalies(rolled, model)
    scaled_reconstructed = reconstructAnomalies(scaled, model)
    rolledAndScaled_reconstructed = reconstructAnomalies(rolledAndScaled, model)
    flattened_reconstructed = reconstructAnomalies(flattened, model)

    # Get anomaly maes and mads
    rolled_maes_perday, rolled_maes_pershift = getAnomalyMaes(rolled, rolled_reconstructed)
    scaled_maes_perday, scaled_maes_perscale = getAnomalyMaes(scaled, scaled_reconstructed)
    rolledAndScaled_maes_perday, rolledAndScaled_maes_perblock = getAnomalyMaes(rolledAndScaled, rolledAndScaled_reconstructed)
    flattened_maes_perday, flattened_maes_perblock = getAnomalyMaes(flattened, flattened_reconstructed)

    rolled_mads = getAnomalyMads(rolled, rolled_reconstructed)
    scaled_mads = getAnomalyMads(scaled, scaled_reconstructed)
    rolledAndScaled_mads = getAnomalyMads(rolledAndScaled, rolledAndScaled_reconstructed)
    flattened_mads = getAnomalyMads(flattened, flattened_reconstructed)

    a={}    
    a['x_test_reconstructed']=x_test_reconstructed
    a['x_train_reconstructed']=x_train_reconstructed
    a['train_maes']=train_maes
    a['threshold_mae']=threshold_mae
    a['train_mads']=train_mads
    a['threshold_mad']=threshold_mad
    a['test_maes']=test_maes
    a['test_mads']=test_mads
    a['rolled']=rolled
    a['scaled']=scaled
    a['rolledAndScaled']=rolledAndScaled
    a['flattened']=flattened
    a['rolled_reconstructed']=rolled_reconstructed
    a['scaled_reconstructed']=scaled_reconstructed
    a['rolledAndScaled_reconstructed']=rolledAndScaled_reconstructed
    a['flattened_reconstructed']=flattened_reconstructed
    a['rolled_maes']=rolled_maes_perday
    a['rolled_mads']=rolled_mads
    a['scaled_maes']=scaled_maes_perday
    a['scaled_mads']=scaled_mads
    a['rolledAndScaled_maes']=rolledAndScaled_maes_perday
    a['rolledAndScaled_mads']=rolledAndScaled_mads
    a['flattened_maes']=flattened_maes_perday
    a['flattened_mads']=flattened_mads

    with open(name+'_anomalies.pickle', 'wb') as h:
        pickle.dump(a,h)

def read_all_saved_data(path, name):
    attributes = dataMunging.load_saved_model(path,name)


    with open(path+'/'+name+'_anomalies.pickle', 'rb') as handle:
        anomalies = pickle.load(handle)
    
    z = {**attributes, **anomalies}
    return z

def get_differences(name1, name2):
    """
    Given two lists, returns the elements that are in one but not 
    in the other. For both elements.
    """

    not2 = list(sorted(set(anomalies[name1])-set(anomalies[name2])))
    not1 = list(sorted(set(anomalies[name2])-set(anomalies[name1])))
    return not2, not1

def plot_reconstructed(name1, name2, position):
    """
    Given two models, plot how the same vector is reconstructed by both.
    """
    plt.figure(1)
    plt.bar(range(24), models[name1]['x_test'][position])
    plt.xticks(np.arange(0, 25.19, 3.0))
    plt.figure(2)
    plt.bar(range(24), models[name1]['x_test_reconstructed'][position])
    plt.xticks(np.arange(0, 25.19, 3.0))
    plt.figure(3)
    plt.bar(range(24), models[name2]['x_test_reconstructed'][position])
    plt.xticks(np.arange(0, 25.19, 3.0))
    plt.show()

# List with the name of the models to consider
names = ['TV_vanilla_12','TV_vanilla_8','TV_vanilla_4',
        'TV_deep_12','TV_deep_8','TV_deep_4']

# For each model, load the relevant information in a dictionary
models = {}
for name in names:
    print('Reading {}...'.format(name))
    attributes = read_all_saved_data('Models/TV', name)    
    models[name] = attributes

# Get all test set anomalies for each model
anomalies = {}
for name in names:
    anomalies[name]= np.where((models[name]['test_mads']>models[name]['threshold_mad'])|(models[name]['test_maes']>models[name]['threshold_mae']))[0]

# Get all rolled anomalies for each model
rolled_anomalies = {}
for name in names:
    vect = []
    for index,_ in enumerate(models[name]['rolled_mads']):
        vect.append(np.where((models[name]['rolled_mads'][index]>models[name]['threshold_mad'])|(models[name]['rolled_maes'][index]>models[name]['threshold_mae']))[0])
    rolled_anomalies[name] = np.array(vect)

# "Mixed" anomalies have the reconstructions of a model 
# but the thresholds of another
mixed = []
for index,_ in enumerate(models['TV_vanilla_12']['rolled_mads']):
    mixed.append(np.where((models['TV_vanilla_4']['rolled_mads'][index]>models['TV_vanilla_12']['threshold_mad'])|(models['TV_vanilla_4']['rolled_maes'][index]>models['TV_vanilla_12']['threshold_mae']))[0])
rolled_anomalies['mixed'] = np.array(mixed)

# Get all scaled anomalies for each model
scaled_anomalies = {}
for name in names:
    vect = []
    for index,_ in enumerate(models[name]['scaled_mads']):
        vect.append(np.where((models[name]['scaled_mads'][index]>models[name]['threshold_mad'])|(models[name]['scaled_maes'][index]>models[name]['threshold_mae']))[0])
    scaled_anomalies[name] = np.array(vect)


# Compute and plot of anomalies per shift in rolled
a = []
for i in rolled_anomalies['TV_vanilla_12']:
    a.append(len(i))
axes = plt.gca()
plt.scatter(range(24),a)
axes.set_ylim([0,211.19])
plt.xlabel('Number of hours shifted')
plt.ylabel('Number of anomalies detected')
plt.show()

# Compute and plot the number of anomalies 
# per shift in rolled for all models
a = []
for i in rolled_anomalies['TV_vanilla_12']:
    a.append(len(i))
axes = plt.gca()
plt.plot(a,color='r', label='Shallow12')
axes.set_ylim([0,211.19])
a = []
for i in rolled_anomalies['TV_vanilla_8']:
    a.append(len(i))
plt.plot(a,color='b', label='Shallow8')
a = []
for i in rolled_anomalies['TV_vanilla_4']:
    a.append(len(i))
plt.plot(a,color='k', label='Shallow4')
a=[]
for i in rolled_anomalies['mixed']:
    a.append(len(i))
plt.plot(a,color='c', label='mixed')
plt.xlabel('Number of hours shifted')
plt.ylabel('Number of anomalies detected')
plt.legend(fontsize=9)
plt.show()


# Plot original vector and the reconstructed
model_name='TV_vanilla_12'
for i in anomalies[model_name]:
    print(i)
    print(models[model_name]['rolled_maes'][10][i]>models[model_name]['threshold_mae'])
    print(str(models[model_name]['rolled_mads'][10][i]>models[model_name]['threshold_mad'])+'\n\n')
    plt.figure(1)
    plt.bar(range(24), models[model_name]['rolled'][10][i])
    plt.figure(2)
    plt.bar(range(24), models[model_name]['rolled_reconstructed'][10][i])
    plt.show()

# Given two models, finds differences in the test set anomalies 
# found by them and plot them.
notdeep, notvanilla = get_differences('TV_vanilla_12', 'TV_deep_12')
for i in notvanilla:
    print(i)
    print('mae > threshold: '+str(models['TV_deep_12']['test_maes'][i]>models['TV_deep_12']['threshold_mae']))
    print('mad > threshold: '+str(models['TV_deep_12']['test_mads'][i]>models['TV_deep_12']['threshold_mad'])+'\n\n')
    plot_reconstructed('TV_vanilla_12', 'TV_deep_12', i)

# Scatterplot of MAEs of the rolled anomalies
for index, name in enumerate(models):    
    plt.subplots()
    plt.scatter(range(24),np.mean(models[name]['rolled_maes'], axis=1))
    plt.axhline(y=models[name]['threshold_mae'], color='r', linestyle='--', label='Anomaly threshold')
    plt.xlabel('Hours shifted')
    plt.ylabel('Average MAE values per shift')
    plt.legend()
    # plt.savefig(name+'.svg')
    plt.show()

# Boxplot of MAEs of the rolled anomalies
for index, name in enumerate(models):
    plt.subplots()
    plt.boxplot([i for i in models[name]['rolled_maes']], positions=range(24), notch=True, bootstrap=5000, whis=[0,95])
    plt.axhline(y=models[name]['threshold_mae'], color='r', linestyle='--', label='Anomaly threshold')
    plt.xlabel('Hours shifted')
    plt.ylabel('Average MAE values per shift')
    plt.xticks(np.arange(0,24.19,4.0), [i for i in range(0,25,4)])
    plt.legend()
    # plt.savefig(name+'_boxplot.svg')
    plt.show()

# Boxplot of MAEs of the scaled anomalies
for index, name in enumerate(models):
    plt.subplots()
    plt.boxplot([i for i in models[name]['scaled_mads']], positions=range(5), notch=True, bootstrap=5000, whis=[0,95])
    plt.axhline(y=models[name]['threshold_mad'], color='r', linestyle='--', label='Anomaly threshold')
    plt.xlabel('Hours shifted')
    plt.ylabel('Average MAE values per shift')
    plt.legend()
    # plt.savefig(name+'_boxplot.svg')
    plt.show()

# Plot relationship between original MAEs and scaled MAEs
indices = models['Microwave_vanilla_12']['test_maes'].argsort()
sortedNormal = models['Microwave_vanilla_12']['test_maes'][indices]
sortedScaled = models['Microwave_vanilla_12']['scaled_maes'][0][indices]
plt.scatter(sortedNormal, sortedScaled)
plt.plot([0,max(sortedNormal)], [0, 0.5*max(sortedNormal)],color='green',label='y = x/2')
plt.xlabel('MAE values for test set')
plt.ylabel('MAE values for test set scaled by 0.5')
plt.legend()
plt.show()

# Scatterplot of "rolled and scaled" anomalies
colors=[]
for i in range(24*5):
    if i%24==0:
        colors.append('C1')
    else:
        colors.append('C0')
plt.scatter(range(24*5), np.mean(models['Microwave_vanilla_12']['rolledAndScaled_maes'],axis=1), c=colors)
plt.axhline(y=models['Microwave_vanilla_12']['threshold_mae'], color='r', linestyle='--', label='Anomaly threshold')
plt.xticks(np.arange(0, 120.19, 6.0), [i%24 for i in range(0,121,6)])
plt.xlabel('Hours shifted')
plt.ylabel('Average MAE values per shift')
plt.legend()
plt.show()

# Plot original and reconstructed flattened vectors considered anomaly
model_name = 'TV_vanilla_12'
pos = np.where((models[model_name]['flattened_mads'][2]<models[model_name]['threshold_mad'])&(models[model_name]['flattened_maes'][2]<models[model_name]['threshold_mae']))[0]
for i in pos:
    print(i)
    print(models[model_name]['flattened_maes'][2][i]>models[model_name]['threshold_mae'])
    print(str(models[model_name]['flattened_mads'][2][i]>models[model_name]['threshold_mad'])+'\n\n')
    plt.figure(1)
    plt.bar(range(24), models[model_name]['flattened'][2][i])
    plt.figure(2)
    plt.bar(range(24), models[model_name]['flattened_reconstructed'][2][i])
    plt.show()


# plot decreasing threshold values for shallow or deep model
model_names=['TV_vanilla_12','TV_vanilla_8','TV_vanilla_4']
th_mae=[]
th_mad=[]
for i in model_names:
    th_mae.append(models[i]['threshold_mae'])
    th_mad.append(models[i]['threshold_mad'])
    

plt.plot([12,8,4],th_mad,'go',linestyle='--', label='MADEs')
plt.plot([12,8,4],th_mae,'bo',linestyle='--', label='MAEs')
plt.xlabel('Bottleneck layer size')
plt.ylabel('Threshold value')
plt.xticks(np.arange(4, 12.19, 4.0))
plt.legend()
plt.show()

# plot two graphs side by side
for i in anomalies['Microwave_vanilla_12']:
    print(i)
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.bar(range(24),models['Microwave_vanilla_12']['x_test'][i])
    ax1.set_title('Original',fontsize=10)
    ax1.set_xticks(np.arange(0, 24, 4.0))
    # ax1.axhline(y=models['TV_vanilla_12']['threshold_mae'], color='r', linestyle='--', label='MAE threshold')
    # ax1.axhline(y=models['TV_vanilla_12']['threshold_mad'], color='tab:orange', linestyle='--', label='MADE threshold')
    # ax1.legend()
    ax2.bar(range(24),models['Microwave_vanilla_12']['x_test_reconstructed'][i])
    ax2.set_title('Shallow12',fontsize=10)
    ax2.set_xticks(np.arange(0, 24.19, 4.0))
    # ax2.axhline(y=models['TV_vanilla_8']['threshold_mae'], color='r', linestyle='--', label='MAE threshold')
    # ax2.axhline(y=models['TV_vanilla_8']['threshold_mad'], color='tab:orange', linestyle='--', label='MADE threshold')
    # ax3.bar(range(24),models['TV_deep_12']['x_test_reconstructed'][51])
    # ax3.set_title('Deep12',fontsize=10)

    fig.text(0.5, 0.04, 'Hour of the day', ha='center', va='center')
    fig.text(0.02, 0.5, 'Energy drawn', ha='center', va='center', rotation='vertical')
    plt.show()

# boxplot deep vs shallow
fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)#, figsize=((12.65*2)/2.54, (12.65)/2.54))
ax1.boxplot([i for i in models['TV_vanilla_12']['rolled_maes']], positions=range(24), notch=True, bootstrap=5000, whis=[0,95])
ax1.axhline(y=models['TV_vanilla_12']['threshold_mae'], color='r', linestyle='--', label='MAE threshold')
ax1.set_title('Shallow12',fontsize=10)
ax1.set_xticks(np.arange(0, 24.19, 4.0))
labels = range(0,25,4)
ax1.set_xticklabels(["{:d}".format(x) for x in labels])
ax1.legend(loc="upper left")

ax2.boxplot([i for i in models['TV_deep_12']['rolled_maes']], positions=range(24), notch=True, bootstrap=5000, whis=[0,95])
ax2.axhline(y=models['TV_deep_12']['threshold_mae'], color='r', linestyle='--', label='MAE threshold')
ax2.set_title('Deep12',fontsize=10)
ax2.set_xticks(np.arange(0, 24.19, 4.0))
labels = range(0,25,4)
ax2.set_xticklabels(["{:d}".format(x) for x in labels])

fig.text(0.5, 0.03, 'Number of hours shifted', ha='center', va='center')
fig.text(0.05, 0.5, 'Average MAE values', ha='center', va='center', rotation='vertical')
# plt.savefig(name+'_boxplot.svg')
# plt.tight_layout()
plt.show()

# plot average day of microwave dataset
with open('Models/Microwave/Microwave_data_sets.pickle', 'rb') as fhand:
    sets = pickle.load(fhand)
plt.bar(range(24), np.mean(sets[0], axis=0), width=0.6, align='edge')
# plt.xticks(np.arange(0, 24, 1.0))
plt.xlabel('Hour of the day')
plt.ylabel('Average Watts')
plt.show()

# Plot Dependency ratio for thesis graph
# This is not relevant for the evaluation of the models
df=pd.read_csv('dependency.csv')
df2=pd.DataFrame()
df2['Years'] = df.columns.values
df2['Values'] = df.values.tolist()[0]
df2=df2.set_index('Years')
df2=df2.reindex(str(year) for year in range(2006,2081))
plt.scatter(df2.index, df2.Values)
xticks = [2006,2010,2015,2020]+list(range(2030,2083,10))
plt.xticks(xticks, xticks, rotation=45)
plt.xlabel('Year')
plt.ylabel('Dependency ratio')
plt.show()

