This is a small guide to the use of my code. It is not an extensive explanation but should be enough to get somebody started. Then, the comments in the Python files should be considered for more specific information. For this guide I assume that the reader is familiar with Python and its tools, and is operating on a UNIX system.

---

## The environment

To set up the environment the easiest way is to use a virtual environment.

1. Start a terminal in the folder where the archive containing the file was extracted.
2. Create a virtual environment with the command: virtualenv -p /usr/local/bin/python3 VirtualEnv
3. Activate the virtual environment with the command: source VirtualEnv/bin/activate
4. Install the required packages with the command: pip install -r requirements.txt

---

## The code

If the virtual environment was set up correctly, to launch an hyperparameter search for the shallow autoencoders one can use the command: python mainHyperparameterSearch.py

Modifying a few lines in that file you can launch the same search for the deep autoencoder.

For the preprocessing steps one can use the same command on the file mainPreprocessing.py. This will not do much though because the original files are not stored in the archive to limit its size. The final datasets are already available in pickle format in the Models folder and can be loaded with the code contained in the mainEvaluate.py file. If one desires to recreate the preprocessing steps from scratch can use the original files in the UK-Dale dataset.

Main evaluate contain code to use in the interactive interactive shell. The file autoencoder contains the classes to create shallow and deep autoencoder. The DataMunging file contains a lot of functions used in the thesis project. Thus, these three files should not be run but called from the interactive shell or another script.

---